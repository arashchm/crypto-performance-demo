import { useMemo, useEffect, useState } from 'react';
import { AESCBC, AESCTR, AESGCM, generateRandomData } from './functions';
import useAsyncEffect from 'use-async-effect';

const repeat = 100;
const oneChunkOfDataSizeInMegabytes = 4;
const totalDataSizeInMegabytes = repeat * oneChunkOfDataSizeInMegabytes;

const App = () => {
    const [bytes, setBytes] = useState(null);
    const [cipher, setCipher] = useState('AES-CBC');
    const [result, setResult] = useState(null);
    const [processing, setProcessing] = useState(false);


    const selectCipherHandler = e => {
        setCipher(e.target.value);
    };

    const runTest = () => {
        setProcessing(true);
        setResult(null);
    };

    const cipherFunction = useMemo(
        () => {
            setResult(null);
            switch (cipher) {
                case 'AES-CBC':
                default:
                    return AESCBC;
                case 'AES-CTR':
                    return AESCTR;
                case 'AES-GCM':
                    return AESGCM;
            }
        },
        [cipher]
    );

    useAsyncEffect(
        async () => {
            if (!processing) return;
            await new Promise(resolve => setTimeout(resolve, 10));
            const result = cipherFunction(bytes, repeat);
            console.log(result);
            setResult(result);
            setProcessing(false);

        },
        [processing]
    );


    useEffect(
        () => {
            // const data = new Blob([new ArrayBuffer(4 * (2 ** 20))], {type: 'application/octet-stream'});
            const data = generateRandomData(oneChunkOfDataSizeInMegabytes * (2 ** 20));
            setBytes(data);

        },
        []
    );

    return (
        <div className="App">
            {
                !processing &&
                <h3>
                    Select cipher
                </h3>
            }
            {
                !processing &&
                <div className='controls'>
                    <input disabled={processing} onChange={selectCipherHandler} type='radio' name='cipher' value='AES-CBC' id='AES-CBC'
                           checked={cipher === 'AES-CBC'}/><label htmlFor='AES-CBC'>AES-CBC</label><br/>
                    <input disabled={processing} onChange={selectCipherHandler} type='radio' name='cipher' value='AES-CTR' id='AES-CTR'
                           checked={cipher === 'AES-CTR'}/><label htmlFor='AES-CTR'>AES-CTR</label><br/>
                    <input disabled={processing} onChange={selectCipherHandler} type='radio' name='cipher' value='AES-GCM' id='AES-GCM'
                           checked={cipher === 'AES-GCM'}/><label htmlFor='AES-GCM'>AES-GCM</label><br/>
                </div>
            }
            {
                !processing &&
                <button type='button' onClick={runTest}>Run</button>
            }
            {
                processing &&
                <p>
                    Running the test, it may take some time.
                </p>
            }
            {
                result &&
                <>
                    <div>
                        Encrypted and decrypted a chunk of data,
                        <span>
                            {
                                ` ${oneChunkOfDataSizeInMegabytes} megabytes in size`
                            }
                        </span>
                        ,
                        <span>
                            {
                                ` ${repeat} times. `
                            }
                        </span>
                        total size:
                        <span>
                            {
                                ` ${totalDataSizeInMegabytes} megabytes.`
                            }
                        </span>
                        <span>
                            {` (cipher: ${cipher})`}
                        </span>
                    </div>
                    <div>
                        Results: encryption took
                        <span>
                            {
                                ` ${(result.totalEncryptTime / 1000).toFixed(2)} seconds, `
                            }
                            {
                                `${(totalDataSizeInMegabytes / result.totalEncryptTime * 1000).toFixed(2)} Mb/s, `
                            }
                        </span>
                        and decryption took
                        <span>
                            {
                                ` ${(result.totalDecryptTime / 1000).toFixed(2)} seconds, `
                            }
                            {
                                `${(totalDataSizeInMegabytes / result.totalDecryptTime * 1000).toFixed(2)} Mb/s.`
                            }
                        </span>
                    </div>
                </>
            }
        </div>
    );
}

export default App;
