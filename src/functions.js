import forge from 'node-forge';

forge.options.usePureJavaScript = true;

export const AESCBC = (bytes, iterations = 1) => {

    const results = {
        totalEncryptTime: 0,
        totalDecryptTime: 0
    }

    const key = forge.random.getBytesSync(32);
    const iv = forge.random.getBytesSync(32);

    for (let i = 0; i < iterations; i++) {
        // encrypt
        const encryptStarted = performance.now();

        const cipher = forge.cipher.createCipher('AES-CBC', key);
        cipher.start({iv: iv});
        cipher.update(forge.util.createBuffer(bytes));
        cipher.finish();

        const encrypted = cipher.output;

        const encryptEnded = performance.now();

        results.totalEncryptTime += encryptEnded - encryptStarted;

        // decrypt
        const decryptStarted = performance.now();

        const decipher = forge.cipher.createDecipher('AES-CBC', key);
        decipher.start({iv: iv});
        decipher.update(encrypted);
        decipher.finish();

        const decryptEnded = performance.now();

        results.totalDecryptTime += decryptEnded - decryptStarted;
    }

    return results;
};

export const AESCTR = (bytes, iterations = 1) => {

    const results = {
        totalEncryptTime: 0,
        totalDecryptTime: 0
    }

    const key = forge.random.getBytesSync(32);
    const iv = forge.random.getBytesSync(32);

    for (let i = 0; i < iterations; i++) {
        // encrypt
        const encryptStarted = performance.now();

        const cipher = forge.cipher.createCipher('AES-CTR', key);
        cipher.start({iv: iv});
        cipher.update(forge.util.createBuffer(bytes));
        cipher.finish();

        const encrypted = cipher.output;

        const encryptEnded = performance.now();

        results.totalEncryptTime += encryptEnded - encryptStarted;

        // decrypt
        const decryptStarted = performance.now();

        const decipher = forge.cipher.createDecipher('AES-CTR', key);
        decipher.start({iv: iv});
        decipher.update(encrypted);
        decipher.finish();

        const decryptEnded = performance.now();

        results.totalDecryptTime += decryptEnded - decryptStarted;
    }

    return results;
};

export const AESGCM = (bytes, iterations = 1) => {

    const results = {
        totalEncryptTime: 0,
        totalDecryptTime: 0
    }

    const key = forge.random.getBytesSync(32);
    const iv = forge.random.getBytesSync(32);

    for (let i = 0; i < iterations; i++) {
        // encrypt
        const encryptStarted = performance.now();

        const cipher = forge.cipher.createCipher('AES-GCM', key);
        cipher.start({
            iv: iv, // should be a 12-byte binary-encoded string or byte buffer
            tagLength: 128 // optional, defaults to 128 bits
        });
        cipher.update(forge.util.createBuffer(bytes));
        cipher.finish();

        const encrypted = cipher.output;
        const tag = cipher.mode.tag;

        const encryptEnded = performance.now();

        results.totalEncryptTime += encryptEnded - encryptStarted;

        // decrypt
        const decryptStarted = performance.now();

        const decipher = forge.cipher.createDecipher('AES-GCM', key);
        decipher.start({
            iv: iv,
            tagLength: 128, // optional, defaults to 128 bits
            tag: tag // authentication tag from encryption
        });
        decipher.update(encrypted);
        decipher.finish();

        const decryptEnded = performance.now();

        results.totalDecryptTime += decryptEnded - decryptStarted;
    }

    return results;
};

export const generateRandomData = size => {
    const chars = 'abcdefghijklmnopqrstuvwxyz'.split('');
    const len = chars.length;
    const random_data = [];

    while (size--) {
        random_data.push(chars[Math.random()*len | 0]);
    }

    return random_data.join('');
}